Intégration Continue : **LaTeX** et **GitLab**.
======= 

- [Démonstration](#d%C3%A9monstration)
- [Explications](#explications)
  - [Prérequis : activer le _shared Runner_](#pr%C3%A9requis-activer-le-shared-runner)
  - [C'est parti](#cest-parti)
      - [Remarques](#remarques)
  - [Commentaires sur l'intégration continue](#commentaires-sur-lint%C3%A9gration-continue)
      - [Autre possibilité](#autre-possibilit%C3%A9)
      - [Pour aller plus loin](#pour-aller-plus-loin)

# Démonstration
Ce _repo_ est en lui-même un démonstration de l'utilisation de l'intégration continue pour la compilation de LaTeX. Le fichier `main.tex` est compilé (voir [Explications](#explications)) et le `pdf` est disponible [ici](https://gitlab.utc.fr/LaTeX-UTC/LaTeX-CI/-/jobs/artifacts/master/raw/main.pdf?job=building-latex-master).

N'hésitez pas à cloner ce _repo_ pour vous en servir comme base.


# Explications 

## Prérequis : activer le _shared Runner_

1. Rendez-vous das les `paramètres` du projet GitLab, section `CI/CD`.
1. Cliquez sur `Runners settings` (_expand_).
1. Dans la section _Shared Runners_, cliquez sur _« Activate shared Runners »_.


## C'est parti 

Vous n'avez plus qu'à modifier le fichier `main.tex` et à _commit_ les changements, la compilation et la publication à l'adresse `https://gitlab.utc.fr/<login-ou-namespace>/<nom-du-projet>/-/jobs/artifacts/master/raw/main.pdf?job=building-latex-master` se fait automatiquement. Par exemple, pour ce projet, le pdf est disponible à l'adresse [https://gitlab.utc.fr/LaTeX-UTC/LaTeX-CI/-/jobs/artifacts/master/raw/main.pdf?job=building-latex-master](https://gitlab.utc.fr/LaTeX-UTC/LaTeX-CI/-/jobs/artifacts/master/raw/main.pdf?job=building-latex-master).


### Remarques 

- Les `pdf` compilés sont rendus disponibles en public via les _artifacts_. Ces derniers peuvent être visualisés via :
  - La page d'accueil du projet, cliquez sur l'icône _télécharger_ puis sur `Télécharger 'building-latex-master'`
  - Dans le menu du projet, cliquez sur `CI/CD` -> `jobs`, puis sur le badge (espérons "réussi") du status associé au job souhaité, puis dans le menu de droite, dans la section `Job artifacts` cliquez sur `browse`.
  - Url : 
      -  Pour la branche master : `https://gitlab.utc.fr/<login-ou-namespace>/<nom-du-projet>/-/jobs/artifacts/master/raw/main.pdf?job=building-latex-master`
      -  Pour les autres branches : `https://gitlab.utc.fr/<login-ou-namespace>/<nom-du-projet>/-/jobs/artifacts/<nom-de-la-branche>/raw/main.pdf?job=building-latex-other-branch`
- Si la compilation échoue, le _job_ sera marqué comme tel et vous pouvez accéder aux logs pour savoir où elle a échouée. 
- _Pensez à adapter la license à votre projet._

## Commentaires sur l'intégration continue
Avec GitLab, toute l'intégration continue est gérée via le fichier `.gitlab-ci.yml`. Voici celui utilisé par ce projet (avec quelques commentaires).

Il comporte 2 _jobs_ sensiblement similaires qui permettent une gestion optimale des ressources disques : à l'heure de la réalisation de ce projet il n'est pas [encore](https://gitlab.com/gitlab-org/gitlab-ce/issues/23777) possible de garder uniquement les derniers artifacts. Pour contourner le système : 
- Les artifacts issus de la branche master sont conservés 2 ans.
- Les artifacts issus des autres branches sont conservés 2 semaines.

```yaml
# Téléchargement d'une image Docker avec tous les packets
# texlive pour éviter les ennuis lors de la compilation latex
image: blang/latex

# Premier job : compilation du latex
building-latex-master:
  stage: build
  script:
    # Compilation avec latexmk du fichier main.tex 
    # qui est à la racine du repo
    - latexmk -pdf main.tex
  artifacts:
    # Génération d'une archive téléchargeable avec le fichier
    paths:
      - "*.pdf"
    expire_in: 2 year # Durée de validité de l'archive
  tags:
    # Tag pour que le shared runner du GitLab de l'UTC 
    # fasse sont travail.
    - docker

# Deuxième job : similaire, mais pour les autres branches
building-latex-other-branch:
  stage: build
  script:
    - latexmk -pdf main.tex
  artifacts:
    paths:
      - "*.pdf"
    expire_in: 2 week # durée plus courte
  except: # toutes les branches sauf master
    - master
  tags:
    - docker
```

### Autre possibilité

Il pourrait être plus simple et moins coûteux en espace disque d'utiliser `GitLab Pages` pour la publication des `PDF` compilés.
**Attention les données disponibles sur GitLab Pages sont publiques même si le projet est privé !**

Voici un exemple de `.gitlab-ci.yml` pour répondre à ce cas d'utilisation :

```yaml
image: blang/latex

building-latex:
  stage: build
  script:
    - latexmk -pdf main.tex
  artifacts:
    paths:
      - "*.pdf"
    expire_in: 2 weeks
  tags:
    - docker

pages:
  stage: deploy
  script:
    - mkdir .public
    - cp main.pdf .public
    - mv .public public
  artifacts:
    paths:
      - public
  only: # publication sur GitLab pages uniquement pour 
    - master   # la branche master
  tags:
    - docker
```
_Dans ce cas, les fichiers sont mis à disposition sur l'URL définie dans `Settings` (du projet) -> `Pages`. Par exemple `url/main.pdf`._ 


### Pour aller plus loin

Dans la partie `script` d'un job décrit dans le fichier `.gitlab-ci.yml` vous pouvez installer des packets (`apt-get install --yes <nom-du-paquet>`) voire exécuter des scripts bash qui seraient disponibles dans le repo :
```yaml
# [...]
building-latex-master:
  stage: build
  script:
    - bash build.sh
# [...]
```

Les possibilités sont donc infinies :)

- Un peu de lecture : https://gitlab.com/help/user/project/pipelines/job_artifacts.md
- Pour tester vos fichiers `.gitlab-ci.yml` : https://gitlab.utc.fr/ci/lint


**Amusez-vous bien.** :wink:


# License

Le contenu de ce _repo_ est mis à disposition sous licence BSD-2, voir [LICENSE](./LICENSE);


Excepté :

Le logo GitLab utilisé dans le dossier `images` est disponible sous license [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/), plus d'informations [ici](https://about.gitlab.com/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines).